FROM runpod/stable-diffusion:comfy-ui-5.0.0

COPY prepForReplicate.sh /prepForReplicate.sh
RUN /bin/bash /prepForReplicate.sh

COPY pre_pre_start.sh /tmp/pre_pre_start.sh
RUN cat /tmp/pre_pre_start.sh >> /pre_start.sh
