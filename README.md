
# modify a runpod ComfyUI container

Start a `Runpod SD ComfyUI` container, then connect and run:

```bash
curl -o - https://gitlab.com/tom_ryan/replicate-comfyui-container/-/raw/main/modifyRunpodContainer.sh?ref_type=heads | bash
```

# building a con

```bash
# configure ubuntu 
curl -o - https://gitlab.com/tom_ryan/replicate-comfyui-container/-/raw/main/configureBuildEnvUbuntu.sh?ref_type=heads | sh

# pull existing container
docker pull iaapita/replicate-comfyui-container

# docker login
docker login

# build a new image
docker build -t iaapita/replicate-comfyui-container .

# push the new image to docker with default `latest` tag
docker push iaapita/replicate-comfyui-container
```

Models are fetched during container startup to avoid having ridiculously huge images.