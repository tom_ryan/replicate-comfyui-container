#!/bin/bash

set -e

# initial apt deps
apt-get update 
apt-get install -y gcc g++ vim jq

# clone replicate repo
cd /workspace
git clone --recurse-submodules https://github.com/fofr/cog-comfyui

# install nodes and models and weights
cd /workspace/cog-comfyui/
./scripts/clone_plugins.sh

# move custom nodes and models to live ComfyUI
mv /workspace/ComfyUI/custom_nodes /workspace/ComfyUI/custom_nodes.dist
mv /workspace/ComfyUI/models /workspace/ComfyUI/models.dist
mv /workspace/cog-comfyui/ComfyUI/{custom_nodes,models} /workspace/ComfyUI/
# put in symlinks in case we want to fetch other models/weights
cd /workspace/cog-comfyui/ComfyUI/
ln -s /workspace/ComfyUI/{custom_nodes,models} .

# restore ComfyUI-Manager. don't restore models - use only from cog
cp -r /workspace/ComfyUI/custom_nodes.dist/ComfyUI-Manager /workspace/ComfyUI/custom_nodes/

# install these requirements before switching to venv, as in cog.yml
curl -o /usr/local/bin/pget -L "https://github.com/replicate/pget/releases/download/v0.6.0/pget_linux_x86_64" && chmod +x /usr/local/bin/pget
pip install onnxruntime-gpu --extra-index-url https://aiinfra.pkgs.visualstudio.com/PublicPackages/_packaging/onnxruntime-cuda-12/pypi/simple/

# install pip pre-reqs from cog.yml
source /venv/bin/activate
pip install opencv-python matplotlib
cd /workspace/cog-comfyui/
pip install `grep -v '#' cog.yaml | sed -e 's%^ *-%%' | tr '\n' \  | grep -Po '(?<=python_packages: ).*(?=run:)'`

# install weights models etc
WEIGHTS=`cat weights.json | jq -r '[.CLIP_VISION, .INSIGHTFACE, .INSTANTID, .IPADAPTER, .VAE][][]' `
python scripts/get_weights.py $WEIGHTS
python scripts/get_weights.py instantid-controlnet.safetensors depth-zoe-xl-v1.0-controlnet.safetensors controlnet-depth-sdxl-1.0.fp16.safetensors Deliberate_v2.safetensors dreamshaper_8.safetensors dreamshaperXL_lightningDPMSDE.safetensors Juggernaut-XL_v9_RunDiffusionPhoto_v2.safetensors 


# install requirements and installers
### don't do this yet... seemed to cause issues
#cd /workspace/ComfyUI/custom_nodes
#for r in */requirements.txt ; do pip install -r $r ; done
#for i in */install.py ; do python $i ; done # must be in venv