cd /cog-comfyui/ComfyUI/
rm /cog-comfyui/ComfyUI/{custom_nodes,models}
ln -s /workspace/ComfyUI/{custom_nodes,models} .
cd /cog-comfyui/
WEIGHTS=`cat weights.json | jq -r '[.CLIP_VISION, .INSIGHTFACE, .INSTANTID, .IPADAPTER, .VAE][][]' `
python scripts/get_weights.py $WEIGHTS
python ./scripts/get_weights.py depth-zoe-xl-v1.0-controlnet.safetensors controlnet-depth-sdxl-1.0.fp16.safetensors albedobaseXL_v21.safetensor anything-v3-fp16-pruned.safetensors Deliberate_v2.safetensors dreamshaper_8.safetensors dreamshaperXL_lightningDPMSDE.safetensors Juggernaut-XL_v9_RunDiffusionPhoto_v2.safetensors RealVisXL_V4.0_Lightning.safetensors sd_xl_turbo_1.0_fp16.safetensors
