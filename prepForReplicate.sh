#!/bin/bash

set -e

apt-get update 
apt-get install -y gcc g++ vim jq
cd /
git clone --recurse-submodules https://github.com/fofr/cog-comfyui
mv /ComfyUI/custom_nodes /ComfyUI/custom_nodes.dist
mv /ComfyUI/models /ComfyUI/models.dist
mv /cog-comfyui/ComfyUI/{custom_nodes,models} /ComfyUI/
cd /cog-comfyui/ComfyUI/
ln -s /ComfyUI/{custom_nodes,models} .
cp -r /ComfyUI/custom_nodes.dist/ComfyUI-Manager /ComfyUI/custom_nodes/
cd /ComfyUI
cp models.dist/checkpoints/* models/checkpoints/
source /venv/bin/activate
pip install opencv-python matplotlib
cd /cog-comfyui
./scripts/clone_plugins.sh
pip install `grep -v '#' cog.yaml | sed -e 's%^ *-%%' | tr '\n' \  | grep -Po '(?<=python_packages: ).*(?=run:)'`
curl -o /usr/local/bin/pget -L "https://github.com/replicate/pget/releases/download/v0.6.0/pget_linux_x86_64" && chmod +x /usr/local/bin/pget
pip install onnxruntime-gpu --extra-index-url https://aiinfra.pkgs.visualstudio.com/PublicPackages/_packaging/onnxruntime-cuda-12/pypi/simple/
cd /ComfyUI/custom_nodes/ComfyUI-Impact-Pack
python install.py
